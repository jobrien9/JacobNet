#include <ESP8266WiFi.h> //https://github.com/esp8266/Arduino/tree/master/libraries/ESP8266WiFi
#include <SimpleDHT.h>
#include <ESP8266HTTPClient.h>

//based on https://github.com/esp8266/Arduino/blob/master/libraries/ESP8266WiFi/examples/HTTPSRequest/HTTPSRequest.ino

//wifi connection parameters
const char *SSID  = "GTother";
const char *PASSWORD = "GeorgeP@1927";

//seconds
const int SAMPLE_RATE_SECONDS = 3;

//API from which to fetch information
const char* host = "api.thingspeak.com";
const int port = 80; //appears to be a default port of sorts
const char* url = "/update?api_key=R3A1ISHM4OXR1LGH&field1=";

byte temperature = 0;
byte humidity = 0;

int pinDHT11 = D2;

SimpleDHT11 dht11;

//using this client to send HTTP requests
WiFiClient client;
WiFiClient slackClient;

void setup(){
    Serial.begin(9600); 
    delay(250); //slight pause to allow for serial monitor to connect
    //this apparently tells it to operate in client mode (as opposed to server)
    WiFi.mode(WIFI_STA);
    //connect to the interwebs
    WiFi.begin(SSID, PASSWORD);
    pinMode(pinDHT11, INPUT);
    Serial.print("Connecting, please wait.");
    promptUser();
   
}

void loop(){
    if (sampleDHT()){
        sendData((int)temperature);
        delay(1000); //pause for a bit
        sendData((int)humidity);

        Serial.println("request finished");
    }
    delay(1000 * SAMPLE_RATE_SECONDS);
}

//helper function to let the user know we are connecting to the internet
void promptUser(){
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }   
    Serial.println("Connected!");
}

//returns true if successful
boolean sampleDHT(){
    Serial.println("=================================");
    Serial.println("Sample DHT11...");

    // read with raw sample data.

    byte data[40] = {0};
    if (dht11.read(pinDHT11, &temperature, &humidity, data)) {
        Serial.print("Read DHT11 failed");
        return false;
    }

    Serial.print("Sample RAW Bits: ");
    for (int i = 0; i < 40; i++) {
        Serial.print((int)data[i]);

        if (i > 0 && ((i + 1) % 4) == 0) {
            Serial.print(' ');
        }
    }

    Serial.print("Sample OK: ");
    Serial.print((int)temperature); Serial.print(" *C, ");
    Serial.print((int)humidity); Serial.println(" %%");

    return true;
}

//helper function to send data
void sendData(String data){
   Serial.println(data);
   if (!client.connect(host, port)) {
       Serial.println("connection to " + String(host) + " failed. You screwed up.");
   }

    client.print("GET " + String(url) + data +
               " HTTP/1.1\r\n" +
              "Host: " + host + "\r\n" +
              "Connection: close\r\n\r\n");
   Serial.println("finished API call");
}

void sendData(int data){
   Serial.println("int print");
   sendData(String(data));
}